
package exercices;

import static donnees.Club.listeDesPersonnes;
import donnees.Personne;
import static utilitaires.UtilTriListe.trierLesPersonnesParNomPrenom;

public class Question01 {
    
   public static void main(String[] args) {
       trierLesPersonnesParNomPrenom(listeDesPersonnes);
         
       System.out.println("Liste des membres du club");
     for ( Personne pers : listeDesPersonnes ){
        
      // il faut traiter les champs de la variable pers individuellement 
      // en utilisant la notation pointée pers.nom 
      // ici on ne récupère que le nom pour l'afficher  
        
         
      System.out.printf("%-20s %-20s %-5skg %-5s %-20s %-5s victoires\n", pers.nom, pers.prenom ,pers.sexe ,pers.poids ,pers.ville , pers.nbVictoires);
        }
       
    }
}






