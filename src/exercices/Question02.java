
package exercices;

import static donnees.Club.listeDesPersonnes;
import donnees.Personne;
import static utilitaires.UtilTriListe.trierLesPersonnesParNomPrenom;

public class Question02 {

    public static void main(String[] args) {
        trierLesPersonnesParNomPrenom(listeDesPersonnes);
        System.out.println("Listes des membres du club de sexe féminin");
         for ( Personne pers : listeDesPersonnes){
           
           //  Ici le traitement appliqué   à la variable pers est conditionnel  
           //  Attention la comparaison de chaîne de caractères doit se faire avec
           //  .equals comme cela apparait ci-dessous
           //  le test doit toujours se mettre entre parenthèses   (  )
              
           if ( pers.sexe.equals("F") ) {
           
              System.out.printf("%-15s %-20s \n",
                                pers.nom,
                                pers.prenom);
                              
                               
                 
    }

    
}
    }}
