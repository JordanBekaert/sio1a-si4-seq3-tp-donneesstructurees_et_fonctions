
package exercices;

import static donnees.Club.listeDesPersonnes;
import donnees.Personne;
import static utilitaires.UtilTriListe.trierLesPersonnesParNomPrenom;

public class Question03 {

    public static void main(String[] args) {
         
        trierLesPersonnesParNomPrenom(listeDesPersonnes);
        System.out.println("Listes des membres du club de sexe féminin");
          
        
     for ( Personne pers : listeDesPersonnes ) {
        
        // On utilise la fonction ageEnAnnees se trouvant dans utilitaires.UtilDate ( import ligne 5)
        // pour calculer l'age correspondant à une date de naissance 
        // on passe le paramètre pers.dateNaiss de type Date
         
        int poids=(pers.poids);

        if ( poids > 80 ){ 
            
          System.out.printf("%-10s %-10s  %2d kg %-20s \n",pers.nom, pers.prenom ,pers.poids, pers.ville);
          
          // la deuxième règle  %2d  indique un entier sur deux positions  
          
        }
     }
     
     System.out.println("\n\n");  
  }      
}
 

